import sys
def is_face_finished(face: list) -> bool:
    single = face[0]
    if(all([i == face[0] for i in face])):
        return True
    return False

def is_cube_finished(cube: list) -> bool:
    for face in cube:
        if not is_face_finished(face):
            return False
    return True

def any_side_finished(cube: list) -> bool:
    for face in cube:
        if is_face_finished(face):
            return True
    return False
