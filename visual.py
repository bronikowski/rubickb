from cube import max_rows_in_cube, Faces
import crayons

colors = ('red', 'blue', 'yellow', 'magenta', 'cyan', 'green')

def show_cube(cube: list):

    print ("\033c")
    max_rows = max_rows_in_cube(cube)
    for i in range(6):
        step = 0
        print("FACE {}/{}".format(Faces(i).name, i))
        for v in cube[i]:
            if(step == max_rows):
                print("")
                step = 0
            crayon = getattr(crayons, colors[v])
            print("{}".format(crayon("■")), end="")
            step = step + 1
        print("\n")


