
import time
import cube
from visual import show_cube
from random import choice, randint
from logic import is_cube_finished, any_side_finished
import sys

move = ['up', 'down', 'left', 'righ']

c = cube.produce_cube(9)

loops = 0
print("\033c")
while True:
    perform = choice(move)
    on_row = randint(1, cube.max_rows_in_cube(c))

    if perform == 'up':
        c = cube.move_vertical(c, on_row, 'up')
    if perform == 'down':
        c = cube.move_vertical(c, on_row, 'down')
    if perform == 'left':
        c = cube.move_horizontal(c, on_row, 'left')
    if perform == 'right':
        c = cube.move_horizontal(c, on_row, 'right')
    show_cube(c)
    loops = loops + 1
    print("permutation {} move {}".format(loops, perform))
    time.sleep(.01)
    if(is_cube_finished(c)):
        sys.exit()
    if(any_side_finished(c)):
        print("Look at it!")
