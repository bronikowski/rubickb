
from enum import Enum
from copy import copy
import math

class Faces(Enum):
    FACE = 0
    LEFT = 1
    RIGHT = 2
    BEHIND = 3
    TOP = 4
    BOTTOM = 5

def produce_side(side_size: int, init_value: int) -> list:
    return [init_value for _ in range(side_size)]

def produce_cube(side_size:int) -> list:
    cube = []
    for i in range(6):
        cube.append(produce_side(side_size, i))
    return cube

MOVE_HORIZONTAL_LEFT = ((0, 1), (1, 3), (3, 2), (2, 0))
MOVE_HORIZONTAL_RIGHT = ((0, 2), (2, 3), (3, 1), (1, 0))
MOVE_VERTICAL_UP = ((0, 4), (4, 3), (3, 5), (5, 0))
MOVE_VERTICAL_DOWN = ((0, 5), (5, 3), (3, 4), (4, 0))

def left() -> list:
    return MOVE_HORIZONTAL_LEFT
def right() -> list:
    return MOVE_HORIZONTAL_RIGHT
def up() -> list:
    return MOVE_VERTICAL_UP
def down() -> list:
    return MOVE_VERTICAL_DOWN

def get_face_values(face: list, pointers: list) -> list:
    values = [face[x] for x in pointers]
    return values

def put_face_values(face: list, pointers: list, values: list) -> list:
    new_face = copy(face)
    for idx, pointer in enumerate(pointers):
        new_face[pointer] = values[idx]
    return new_face

def max_rows_in_cube(cube: list) -> int:
    return int(math.sqrt(len(cube[0])))

def move_vertical(cube: list, row: int, direction: str):

    matrix = globals()[direction]()

    new_cube = copy(cube)

    max_rows = max_rows_in_cube(cube)
    if(row > max_rows):
        raise ValueError("Max number of rows is {}".format(max_rows))

    """
    012
    345
    678

    1 = [0, 3, 6]
    2 = [1, 4, 7]
    3 = [2, 5, 8]
    """

    pointers = []
    pointer = (row-1)
    for i in range(max_rows):
        pointers.append(pointer)
        pointer = pointer + max_rows

    for move_from, move_to in matrix:
        data_from = get_face_values(cube[move_from], pointers)
        new_cube[move_to] = put_face_values(cube[move_to], pointers, data_from)

    return new_cube


def move_horizontal(cube: list, row: int, direction: str) -> list:
    
    matrix = globals()[direction]()

    new_cube = copy(cube)

    max_rows = max_rows_in_cube(cube)
    if(row > max_rows):
        raise ValueError("Max number of rows is {}".format(max_rows))

    pointers = []
    pointer = (row-1)*max_rows
    for i in range(max_rows):
        pointers.append(pointer)
        pointer = pointer + 1
    for move_from, move_to in matrix:
        data_from = get_face_values(cube[move_from], pointers)
        new_cube[move_to] = put_face_values(cube[move_to], pointers, data_from)

    return new_cube


